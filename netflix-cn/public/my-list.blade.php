
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Netflix App</title>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
</head>
<body style="background-color:rgb(26, 26, 26)">
<nav class="navbar navbar-expand-lg navbar-dark bg-rgb(26, 26, 26)">
  <a class="navbar-brand font-weight-bold" href="#">Netflix-Ex</a>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">TV Shows</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Movies</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Latest</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="my-list.blade.php">My List</a>
      </li>
    </ul>
  </div>
  <form class="form-inline md-form mr-auto mb-4">
  <input class="form-control mr-sm-2" type="text" name="country" id="country" placeholder="Search" aria-label="Search">
  <img src="avatar.png" alt="avatar" class="img-circle">
</form>
</nav>

<?php
session_start();
$product_ids = array();
//session_destroy();
//
if(filter_input(INPUT_POST, 'add_to_cart')){
    if(isset($_SESSION['shopping_cart'])){
        
     $count = count($_SESSION["shopping_cart"]);

        $product_ids = array_column($_SESSION['shopping_cart'], 'id');

        if (!in_array(filter_input(INPUT_GET, 'id'), $product_ids)){
            $_SESSION['shopping_cart'][$count] = array
        (
            'id'=>filter_input(INPUT_GET,'id'),  
            'name'=>filter_input(INPUT_POST,'name'),
            'image'=>filter_input(INPUT_POST,'image')  
        );
        }
    }
    else{
        $_SESSION['shopping_cart'][0] = array
        (
            'id'=>filter_input(INPUT_GET,'id'),  
            'name'=>filter_input(INPUT_GET,'name'),
            'image'=>filter_input(INPUT_POST,'image')   
        );
    }
}
     if(filter_input(INPUT_GET, 'action') == 'delete'){
           foreach($_SESSION['shopping_cart'] as $key => $product){
                if ($product['id'] == filter_input(INPUT_GET, 'id')){
                     unset($_SESSION['shopping_cart'][$key]);
                }
           }
           $_SESSION['shopping_cart'] = array_values($_SESSION['shopping_cart']);
     }
//pre_r($_SESSION);

function pre_r($product){
    echo '<pre>';
    print_r($product);
    echo '</pre>';
}
?>
<div class="container mx-auto px-4 pt-16">
        <div class="popular-movies">
            <h2 class="uppercase tracking-wider text-light text-lg font-weight-bold">MY LIST</h2>
            <div class="row grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-5">
            <?php
 $connect = mysqli_connect("localhost", "root", "", "movies-list");
 $query = "SELECT * FROM mylist";
 $result = mysqli_query($connect, $query);

if($result){
    if(mysqli_num_rows($result)>0){
        while($product = mysqli_fetch_assoc($result)){
            //print_r($product);
            ?>
           
                    

                <?php
        
        }
    }
}
?>

            </div>
        </div>
    </div>
 
                <div class="container table-responsive">  
                     <table class="table table-bordered text-white">  
                          <tr>  
                              <th width="5%">id</th> 
                              <th width="10%">Movie Name</th> 
                               <th width="40%">image</th>   
                          </tr>  
                          <?php   
                          if(!empty($_SESSION["shopping_cart"]))  
                          {  
                               foreach($_SESSION["shopping_cart"] as $key => $values)  
                               {  
                          ?>  
                          <tr>
                          <h5 class="font-weight-bold text-light"><?php echo $product["name"]; ?></h5>
                              <td><?php echo $values["id"]; ?></td>
                               <td><?php echo $values["name"]; ?></td>  
                               <td><?php echo $values["image"]; ?></td>  
                               <td><a href="my-list.blade.php?action=delete&id=<?php echo $values["id"]; ?>"><span class="text-danger">Remove</span></a></td>  
                          </tr>  
                          <?php  
                                    //$total = $total + ($values["item_quantity"] * $values["item_price"]);  
                               }  
                          ?>  
                          
                          <?php  
                          }  
                          ?>  
                     </table>  
                </div>  
           </div>  

     <script>
$(document).ready(function(){
 
 $('#country').typeahead({
  source: function(query, result)
  {
   $.ajax({
    url:"fetch.php",
    method:"POST",
    data:{query:query},
    dataType:"json",
    success:function(data)
    {
     result($.map(data, function(item){
      return item;
     }));
    }
   })
  }
 });
 
});
</script>
</body>
</html>