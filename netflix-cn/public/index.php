<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Netflix App</title>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
</head>
<body style="background-color:rgb(26, 26, 26)">
<nav class="navbar navbar-expand-lg navbar-dark bg-rgb(26, 26, 26)">
  <a class="navbar-brand font-weight-bold" href="#">Netflix-Ex</a>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">TV Shows</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Movies</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Latest</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="my-list.blade.php">My List</a>
      </li>
    </ul>
  </div>
  <form class="form-inline md-form mr-auto mb-4">
  <input class="form-control mr-sm-2" type="text" name="country" id="country" placeholder="Search" aria-label="Search">
  <img src="avatar.png" alt="avatar" class="img-circle">
</form>
</nav>

<?php
session_start();
$product_ids = array();
//session_destroy();
//
if(filter_input(INPUT_POST, 'add_to_cart')){
    if(isset($_SESSION['shopping_cart'])){
        $count = count($_SESSION["shopping_cart"]);

         $product_ids = array_column($_SESSION['shopping_cart'], 'id');

         if (!in_array(filter_input(INPUT_GET, 'id'), $product_ids)){
             $_SESSION['shopping_cart'][$count] = array
         (
             'id'=>filter_input(INPUT_GET,'id'),  
             'name'=>filter_input(INPUT_POST,'name'),
             'image'=>filter_input(INPUT_POST,'image')  
         );
         }else{
             for ($i =0; $i < count($product_ids); $i++){
                if ($product_ids[$i] == filter_input(INPUT_GET,'id')){
                     $_SESSION['shopping_cart'][$i]['name'] += filter_input(INPUT_POST, 'name');
                 }
             }
         }
    }else{
        $_SESSION['shopping_cart'][0] = array
        (
            'id'=>filter_input(INPUT_GET,'id'),  
            'name'=>filter_input(INPUT_GET,'p_name'),
            'image'=>filter_input(INPUT_POST,'image')   
        );
    }
}
//pre_r($_SESSION);

function pre_r($array){
    echo '<pre>';
    print_r($array);
    echo '</pre>';
}
?>
<div class="container mx-auto px-4 pt-16">
        <div class="popular-movies">
            <h2 class="uppercase tracking-wider text-light text-lg font-weight-bold">POPULAR MOVIES</h2>
            <div class="row grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-5">
            <?php
$connect = mysqli_connect("localhost", "root", "", "movies-list");
$query = 'SELECT * FROM mylist ORDER by id ASC';
$result = mysqli_query($connect, $query);

if($result){
    if(mysqli_num_rows($result)>0){
        while($product = mysqli_fetch_assoc($result)){
            //print_r($product);
            ?>
            <div class="col-3">
            <form method="post" action="my-list.blade.php?action=add&id=<?php echo $product["id"]; ?>">
                    <a href="show.blade.php">
                        <img src="<?php echo $product["image"]; ?>" class="img-responsive"/>
                        <h5 class="font-weight-bold text-light"><?php echo $product["name"]; ?></h5>
                    <!-- <p class="mt-0 text-light">Jan 20 2020</p> -->
                    <button type="submit" name="add_to_cart" class="btn btn-info btn-danger mb-4" value="Add to Cart">Add to My List</button>
                    </a>  
                    </form>                
                </div>
                <?php
        
        }
    }
}
?>
            </div>
        </div>
    </div>

     <script>
$(document).ready(function(){
 
 $('#country').typeahead({
  source: function(query, result)
  {
   $.ajax({
    url:"fetch.php",
    method:"POST",
    data:{query:query},
    dataType:"json",
    success:function(data)
    {
     result($.map(data, function(item){
      return item;
     }));
    }
   })
  }
 });
 
});
</script>
</body>
</html>